ui = undefined;
uiIndicators = undefined;
indicatorsHeight = 0;

function initUi() {
    ui = document.getElementById('ui');
    indicatorsHeight = Object.keys(charts.indicators).length > 0
        ? Object.values(charts.indicators).map(indicator => indicator.height).reduce((a,b) => a + b) : 0;
    indicatorsHeight += Object.keys(charts.indicators).length * (TOP_OFFSET + BOTTOM_OFFSET + 2);

    const height = (charts.candles.height + TOP_OFFSET + TOP_OFFSET + charts.timeScale.height
        + indicatorsHeight) + 'px';
    ui.setAttribute('style', 'top: ' + charts.candles.c.offsetTop + 'px; height: ' + height + '; width: ' + MAX_CANVAS_WIDTH + 'px');
    ui.innerHTML = "";

    if (currencyPairsDropdown.values.length === 0) {
        currencyPairsDropdown.values = currencyPairs;
    }
    if (addIndicatorDropdown.values.length === 0) {
        addIndicatorDropdown.values = Object.keys(indicatorTemplates);
        addIndicatorDropdown.labels = Object.values(indicatorTemplates).map(indicator => indicator.name);
    }
    uiInitLastPriceIndicator();
    uiInitCursorPriceIndicator();
    uiInitZone();
    x = uiAddDropdown('currencyPairSelected', 0, 4, 20, currencyPairsDropdown);
    uiAddDropdown('periodSelected', x + 10, 4, 20, periodDropdown);
    uiAddLabel('openCandleLabel', 'O', 200, 23, 12);
    uiAddLabel('highCandleLabel', 'H', 320, 23, 12);
    uiAddLabel('lowCandleLabel', 'L', 440, 23, 12);
    uiAddLabel('closeCandleLabel', 'C', 560, 23, 12);

    uiIndicators = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    ui.appendChild(uiIndicators);
    uiInitIndicators();
}

function uiShowZone(fromId, toId) {
    toId = toId !== undefined ? toId : fromId;
    sx = fromId * conf.candleWidth;
    width = (toId - fromId + 1) * conf.candleWidth - 1;
    if (width < 0.2) width = 0.2;
    cx = sx + (width / 2);
    if (toId + conf.showCandleFrom < candles.length) {
        candle = candles[toId + conf.showCandleFrom];

        if (candle !== undefined) {
            const zone = document.getElementById('zoneLine');
            const zoneRect = document.getElementById('zoneRect');
            const zoneTextDate = document.getElementById('zoneTextDate');
            const zoneTextHour = document.getElementById('zoneTextHour');

            zone.setAttribute('x', sx);
            zone.setAttribute('width', width);
            zone.setAttribute('height', charts.candles.height + TOP_OFFSET + BOTTOM_OFFSET + indicatorsHeight);
            zoneRect.setAttribute('x', cx - 32);
            zoneTextDate.setAttribute('x', cx - 32 + 6);
            zoneTextHour.setAttribute('x', cx - 32 + 18);

            const isoDate = new Date(candle.date * 1000).toISOString();
            const date = isoDate.substring(0, 10);
            const hour = isoDate.substring(11, 19);
            zoneTextDate.innerHTML = date;
            zoneTextHour.innerHTML = hour;
        }
    }
}

function uiInitLastPriceIndicator() {
    const lastCandle = candles[candles.length - 1];
    x2 = MAX_CANVAS_WIDTH - conf.scaleCanvasWidth;
    y = valueToY(charts.candles, lastCandle.close);
    colorDark = lastCandle.open > lastCandle.close ? COLOR('DARK_RED') : COLOR('DARK_GREEN');
    color = lastCandle.open > lastCandle.close ? COLOR('RED') : COLOR('GREEN');

    const r = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    r.id = 'lastPriceIndicatorRect';
    r.setAttribute('x', x2);
    r.setAttribute('y', y - 6);
    r.setAttribute('width', 1);
    r.setAttribute('height', 14);
    r.setAttribute('fill', colorDark);
    ui.appendChild(r);

    const t = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    t.id = 'lastPriceIndicatorText';
    t.setAttribute('x', x2 + 6);
    t.setAttribute('y', y + 5);
    t.setAttribute('font-size', 11);
    t.setAttribute('fill', COLOR('SECONDARY'));
    t.innerHTML = '12345.12345678';
    ui.appendChild(t);
    r.setAttribute('width', t.getComputedTextLength() + 12);


    const line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    line.id = 'lastPriceIndicatorLine';
    line.setAttribute('x1', 0);
    line.setAttribute('x2', x2 - 2);
    line.setAttribute('y1', y);
    line.setAttribute('y2', y + 1);
    line.setAttribute('stroke', color);
    line.setAttribute('stroke-width', '2');
    line.setAttribute('stroke-dasharray', '10,4');
    ui.appendChild(line);
}

function uiInitCursorPriceIndicator() {
    x2 = MAX_CANVAS_WIDTH - conf.scaleCanvasWidth;

    const r = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    r.id = 'cursorPriceIndicatorRect';
    r.setAttribute('x', x2);
    r.setAttribute('y', y - 6);
    r.setAttribute('width', 1);
    r.setAttribute('height', 14);
    r.setAttribute('fill', COLOR('SECONDARY'));
    ui.appendChild(r);

    const t = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    t.id = 'cursorPriceIndicatorText';
    t.setAttribute('x', x2 + 6);
    t.setAttribute('y', 12);
    t.setAttribute('font-size', 11);
    t.setAttribute('fill', COLOR('PRIMARY'));
    t.innerHTML = '12345.12345678';
    ui.appendChild(t);
    r.setAttribute('width', t.getComputedTextLength() + 12);

    const line = document.createElementNS("http://www.w3.org/2000/svg", 'line');
    line.id = 'cursorPriceIndicatorLine';
    line.setAttribute('x1', 0);
    line.setAttribute('x2', x2 - 2);
    line.setAttribute('y1', y);
    line.setAttribute('y2', y + 1);
    line.setAttribute('stroke', COLOR('SECONDARY'));
    line.setAttribute('stroke-width', '2');
    line.setAttribute('stroke-dasharray', '10,4');
    ui.appendChild(line);
}

function uiUpdateLastPrice(candle) {
    const line = document.getElementById('lastPriceIndicatorLine');
    const rect = document.getElementById('lastPriceIndicatorRect');
    const text = document.getElementById('lastPriceIndicatorText');
    if (line !== null && rect !== null && text !== null) {
        y = valueToY(charts.candles, candle.close);
        if (y > charts.candles.height + TOP_OFFSET + BOTTOM_OFFSET) {
            // otherwise, the indicator might be shown over some external indicator's canvas
            y = 9999999;
        }
        colorDark = candle.open > candle.close ? COLOR('DARK_RED') : COLOR('DARK_GREEN');
        color = candle.open > candle.close ? COLOR('RED', 0.5) : COLOR('GREEN', 0.5);
        line.setAttribute('y1', y);
        line.setAttribute('y2', y + 1);
        line.setAttribute('stroke', color);
        rect.setAttribute('y', y - 6);
        text.setAttribute('y', y + 5);
        text.innerHTML = Number.parseFloat(candle.close).toFixed(8);
        rect.setAttribute('width', text.getComputedTextLength() + 12);
    }
}

function uiGetScaleLabel(y) {
    let acc = 0;
    let places = [];
    let owners = [];

    if (y > TOP_OFFSET && y < charts.candles.height + TOP_OFFSET) {
        return yToValue(charts.candles, y - TOP_OFFSET);
    }
    let accY = TOP_OFFSET + charts.candles.height + BOTTOM_OFFSET + 2;
    chartIndicators = Object.keys(charts.indicators);
    for (let i = 0; i < chartIndicators.length; ++i) {
        if (chartIndicators[i] !== undefined) {
            chartIndicator = charts.indicators[chartIndicators[i]];
            if ((y - accY) > TOP_OFFSET && (y - accY) < chartIndicator.height + TOP_OFFSET) {
                return yToValue(chartIndicator, (y - accY) - TOP_OFFSET);
            }
            accY += TOP_OFFSET + chartIndicator.height + BOTTOM_OFFSET + 2;
        }
    };

    return undefined;
}

function uiUpdateCursorPrice(clientY) {
    y = clientY - charts.candles.c.offsetTop;
    label = uiGetScaleLabel(y);
    if (label === undefined) {
        y = 9999999;
    } else {
        const line = document.getElementById('cursorPriceIndicatorLine');
        if (line !== null) {
            const rect = document.getElementById('cursorPriceIndicatorRect');
            const text = document.getElementById('cursorPriceIndicatorText');
            line.setAttribute('y1', y);
            line.setAttribute('y2', y + 1);
            text.setAttribute('y', y + 5);
            text.innerHTML = label;
            rect.setAttribute('y', y - 6);
            rect.setAttribute('width', text.getComputedTextLength() + 12);
        }
    }
}

function uiInitZone() {
    y = charts.candles.height + TOP_OFFSET + BOTTOM_OFFSET + 2 + indicatorsHeight;

    const zone = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    zone.id = 'zoneLine';
    zone.setAttribute('x', 0);
    zone.setAttribute('y', 0);
    zone.setAttribute('fill', COLOR('SECONDARY', 0.1));
    zone.setAttribute('width', conf.candleWidth);
    zone.setAttribute('height', charts.candles.height + TOP_OFFSET + BOTTOM_OFFSET + indicatorsHeight);
    ui.appendChild(zone);

    const r = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    r.id = 'zoneRect';
    r.setAttribute('x', 40);
    r.setAttribute('y', y);
    r.setAttribute('width', 100);
    r.setAttribute('height', 30);
    r.setAttribute('fill', COLOR('SECONDARY'));
    ui.appendChild(r);

    const t = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    t.id = 'zoneTextDate';
    t.setAttribute('x', 46);
    t.setAttribute('y', y + 10);
    t.setAttribute('font-size', 11);
    t.setAttribute('fill', COLOR('PRIMARY'));
    t.innerHTML = '2019-07-14';
    ui.appendChild(t);
    r.setAttribute('width', t.getComputedTextLength() + 12);

    const t2 = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    t2.id = 'zoneTextHour';
    t2.setAttribute('x', 58);
    t2.setAttribute('y', y + 24);
    t2.setAttribute('font-size', 11);
    t2.setAttribute('fill', COLOR('PRIMARY'));
    t2.innerHTML = '22:00:00';
    ui.appendChild(t2);
}

function uiGetIndicatorY(indicator) {
    let y = charts.candles.height + TOP_OFFSET + BOTTOM_OFFSET + 2;
    const chartIndicatorsKeys = Object.keys(charts.indicators);
    for (let i = 0; i < chartIndicatorsKeys.length; ++i) {
        if (chartIndicatorsKeys[i] === indicator.getId()) {
            break;
        }
        y += charts.indicators[chartIndicatorsKeys[i]].height + TOP_OFFSET + BOTTOM_OFFSET + 2;
    }
    return y;
}

function uiInitIndicators() {
    uiIndicators.innerHTML = "";
    indicatorsHeight = Object.keys(charts.indicators).length > 0
        ? Object.values(charts.indicators).map(indicator => indicator.height).reduce((a,b) => a + b) : 0;
    indicatorsHeight += Object.keys(charts.indicators).length * (TOP_OFFSET + BOTTOM_OFFSET + 2);
    let idx = 0;
    indicators.forEach(indicator => {
        if (indicatorTemplates[indicator.type].display.external) {
            y = uiGetIndicatorY(indicator) + 16;
        } else {
            y = idx * 17 + 42;
            ++idx;
        }
        uiAddIndicator(indicator, 4, y);
    });
    const gAddIndicator = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    uiIndicators.appendChild(gAddIndicator);
    gAddIndicator.onclick = function (ev) {
        initDialog(addIndicatorDropdown, ev.clientX - 20, ev.clientY - 20);
    }

    const rectAddIndicator = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    rectAddIndicator.setAttribute('x', 0);
    rectAddIndicator.setAttribute('y', idx * 17 + 31);
    rectAddIndicator.setAttribute('width', 17);
    rectAddIndicator.setAttribute('height', 12);
    rectAddIndicator.setAttribute('fill', COLOR('PRIMARY', 0.8));
    gAddIndicator.appendChild(rectAddIndicator);

    const textAddIndicator = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    textAddIndicator.setAttribute('x', 4);
    textAddIndicator.setAttribute('y', idx * 17 + 42);
    textAddIndicator.setAttribute('fill', COLOR('SECONDARY'));
    textAddIndicator.innerHTML = "+";
    gAddIndicator.appendChild(textAddIndicator);
}

function uiAddIndicator(indicator, sx, sy) {
    const g = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    g.id = 'indicator' + indicator.uniq + 'g';
    uiIndicators.appendChild(g);

    const r = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    r.setAttribute('x', sx - 4);
    r.setAttribute('y', sy - 11);
    r.setAttribute('width', 400);
    r.setAttribute('height', 14);
    r.setAttribute('fill', COLOR('PRIMARY', 0.8));
    g.appendChild(r);

    const textLabel = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    textLabel.id = 'indicator' + indicator.uniq + 'short';
    textLabel.setAttribute('x', sx);
    textLabel.setAttribute('y', sy);
    textLabel.setAttribute('font-size', 11);
    textLabel.setAttribute('fill', COLOR('SECONDARY'));
    textLabel.innerHTML = indicator.getShortName();
    textLabel.ondblclick = function (ev) {
        initSettingsDialog(indicator);
    }
    g.appendChild(textLabel);

    const hideIcon = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    hideIcon.setAttribute('d', 'M0 0v14h14V0zm7 2.69c3.018 0 5.172 3.232 5.172 4.31 0 1.077-2.154 4.31-5.172 4.31S1.828 8.08 1.828 7c0-1.077 2.154-4.31 5.172-4.31zm0 1.508C5.49 4.198 4.198 5.49 4.198 7S5.49 9.802 7 9.802 9.802 8.51 9.802 7 8.51 4.198 7 4.198zm0 1.68c.646 0 1.12.476 1.12 1.122 0 .646-.473 1.12-1.12 1.12-.646 0-1.12-.473-1.12-1.12 0-.646.473-1.12 1.12-1.12z');
    hideIcon.setAttribute('fill',  COLOR('SECONDARY', indicator.show ? 1 : 0.5));
    hideIcon.setAttribute('stroke',  COLOR('PRIMARY'));
    const scaleHideIcon = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    scaleHideIcon.setAttribute('transform', 'scale(0.8)');
    scaleHideIcon.appendChild(hideIcon);
    const translateHideIcon = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    const hideIconX = sx + textLabel.getComputedTextLength() + 8;
    translateHideIcon.setAttribute('transform', 'translate(' + hideIconX + ', ' + (sy - 9) + ')');
    translateHideIcon.appendChild(scaleHideIcon);
    translateHideIcon.onclick = function(ev) {
        indicator.show = !indicator.show;

        textLabel.setAttribute('fill', COLOR('SECONDARY', indicator.show ? 1 : 0.5));
        hideIcon.setAttribute('fill',  COLOR(indicator.show ? 'SECONDARY' : 'BLUE_SKY'));
        refreshDisplay();
    };
    g.appendChild(translateHideIcon);

    const settingsIcon = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    settingsIcon.setAttribute('d', 'M-2.4 120.9v14h14v-14zm6.086 1.803H5.52v1.332c.416.1.805.258 1.166.48l.944-.935 1.297 1.297-.943.943c.215.35.38.748.48 1.164H9.8v1.837H8.463c-.1.417-.257.806-.48 1.167l.935.944-1.296 1.298-.944-.943c-.35.215-.747.38-1.164.48v1.332H3.677v-1.33c-.415-.102-.804-.258-1.165-.482l-.943.936-1.298-1.296.94-.945c-.216-.35-.38-.748-.482-1.165H-.597v-1.835H.737c.1-.416.257-.805.48-1.166l-.935-.944 1.296-1.297.944.943c.35-.215.747-.38 1.164-.48zm.912 3.053c-1.188 0-2.143.963-2.143 2.143 0 1.187.963 2.143 2.143 2.143 1.18 0 2.14-.963 2.145-2.145 0-1.188-.966-2.144-2.145-2.144z');
    settingsIcon.setAttribute('fill',  COLOR('SECONDARY'));
    settingsIcon.setAttribute('stroke',  COLOR('PRIMARY'));
    const scaleSettingsIcon = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    scaleSettingsIcon.setAttribute('transform', 'scale(0.8)');
    scaleSettingsIcon.appendChild(settingsIcon);
    const translateSettingsIcon = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    translateSettingsIcon.setAttribute('transform', 'translate(' + (hideIconX + 16) + ', ' + (sy - 105.5) + ')');
    translateSettingsIcon.appendChild(scaleSettingsIcon);
    translateSettingsIcon.onclick = function(ev) {
        initSettingsDialog(indicator);
    };
    g.appendChild(translateSettingsIcon);

    const deleteIcon = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    deleteIcon.setAttribute('d', 'M-2.4 120.9v14h14v-14zm3.34 2.123l3.66 3.66 3.66-3.66 1.217 1.22-3.66 3.658 3.66 3.66-1.22 1.22-3.658-3.66-3.66 3.66-1.22-1.22 3.66-3.66-3.66-3.66z');
    deleteIcon.setAttribute('fill',  COLOR('SECONDARY'));
    deleteIcon.setAttribute('stroke',  COLOR('PRIMARY'));
    const scaleDeleteIcon = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    scaleDeleteIcon.setAttribute('transform', 'scale(0.8)');
    scaleDeleteIcon.appendChild(deleteIcon);
    const translateDeleteIcon = document.createElementNS("http://www.w3.org/2000/svg", 'g');
    translateDeleteIcon.setAttribute('transform', 'translate(' + (hideIconX + 30) + ', ' + (sy - 105.5) + ')');
    translateDeleteIcon.appendChild(scaleDeleteIcon);
    translateDeleteIcon.onclick = function(ev) {
        indicators.splice(indicators.indexOf(indicator), 1);
        uiInitIndicators();
        refreshDisplay();
    };
    g.appendChild(translateDeleteIcon);


    let labelX = hideIconX + 50;
    const keys = Object.keys(indicatorTemplates[indicator.type].display.charts);
    for (let i = 0; i < keys.length; ++i) {
        const paramValue = 12345; // indicator.data[keys[i]].length;
        const paramLabel = document.createElementNS("http://www.w3.org/2000/svg", 'text');
        paramLabel.id = 'indicator' + indicator.uniq + 'data' + keys[i];
        paramLabel.setAttribute('x', labelX);
        paramLabel.setAttribute('y', sy + 1);
        paramLabel.setAttribute('font-size', 11);
        const c = COLOR(indicator.displayParams[keys[i]].color);
        paramLabel.setAttribute('fill', c);
        const label = Number.parseFloat(paramValue).toFixed(8);
        paramLabel.innerHTML = label === 'NaN' ? paramValue : label;
        g.appendChild(paramLabel);
        labelX += paramLabel.getComputedTextLength() + 8;
    }

    r.setAttribute('width', labelX);
}

function uiAddLabel(id, label, sx, sy, size) {
    const r = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    r.setAttribute('x', sx - 4);
    r.setAttribute('y', sy - 12);
    r.setAttribute('width', 110);
    r.setAttribute('height', 15);
    r.setAttribute('fill', COLOR('PRIMARY', 0.8));

    ui.appendChild(r);
    const textLabel = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    textLabel.setAttribute('x', sx);
    textLabel.setAttribute('y', sy);
    textLabel.setAttribute('font-size', size);
    textLabel.setAttribute('fill', COLOR('SECONDARY'));
    textLabel.innerHTML = label;
    ui.appendChild(textLabel);

    const valueLabel = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    valueLabel.id = id;
    valueLabel.setAttribute('x', sx + textLabel.getComputedTextLength() + 4);
    valueLabel.setAttribute('y', sy);
    valueLabel.setAttribute('font-size', size);
    valueLabel.setAttribute('fill', COLOR('RED'));
    valueLabel.innerHTML = 12345.12345678;
    ui.appendChild(valueLabel);

    return textLabel.getComputedTextLength() + valueLabel.getComputedTextLength() + 4; 
}

function uiAddDropdown(id, sx, sy, size, dropdownData) {
    boxOffset = 4;
    sx += boxOffset;
    sy += boxOffset;
    const text = document.createElementNS("http://www.w3.org/2000/svg", 'text');
    text.setAttribute('x', sx + size);
    text.setAttribute('y', sy + size * 0.75);
    text.setAttribute('font-size', size);
    text.setAttribute('fill', COLOR('SECONDARY'));
    text.innerHTML = dropdownData.getLabel(dropdownData, dropdownData.selected);
    text.id = id;

    const arrow = document.createElementNS("http://www.w3.org/2000/svg", 'path');
    arWidth = size * 0.75;
    arHeight = size * 0.6;
    arrow.setAttribute('d', 'M'+sx+' '+(sy+size*0.2)+'L'+(sx+arWidth)+' '+(sy+size*0.2)+' L'+(sx+arWidth/2)+' '+(sy+arHeight)+' Z');
    arrow.setAttribute('fill',  COLOR('SECONDARY'));

    ui.appendChild(text);
    ui.appendChild(arrow);

    const totalWidth = size + text.getComputedTextLength() + 8;
    const r = document.createElementNS("http://www.w3.org/2000/svg", 'rect');
    r.setAttribute('y', sy - boxOffset);
    r.setAttribute('x', sx - boxOffset);
    r.setAttribute('fill', COLOR('SECONDARY', 0.1));
    r.setAttribute('fill-opacity', 0);
    r.setAttribute('stroke-opacity', 0);
    r.setAttribute('stroke', 'white');
    r.setAttribute('width', totalWidth);
    r.setAttribute('height', size + 4);
    r.onmouseenter = function(ev) {
        r.setAttribute('fill-opacity', 1);
        r.setAttribute('stroke-opacity', 1);
    }
    r.onmouseleave = function(ev) {
        r.setAttribute('fill-opacity', 0);
        r.setAttribute('stroke-opacity', 0);
    }
    r.onclick = function(ev) {
        initDialog(dropdownData, ev.clientX - 20, ev.clientY - 20);
    }
    ui.appendChild(r);

    return totalWidth;
}

function initSettingsDialog(indicator) {
    const table = document.getElementById('dialogSettingsTable');
    table.innerHTML = "";

    const paramsKeys = Object.keys(indicator.params);
    paramsKeys.forEach(paramKey => {
        const tr = document.createElement('tr');
        table.appendChild(tr);

        const tdLabel = document.createElement('td');
        tdLabel.innerHTML = paramKey;
        tr.appendChild(tdLabel);

        const tdInput = document.createElement('td');
        const input = document.createElement('input');
        input.setAttribute('value', indicator.params[paramKey]);
        input.onchange = function (ev) {
            indicator.params[paramKey] = input.value;
            updateIndicatorData(indicator);
        }
        tdInput.appendChild(input);
        tr.appendChild(tdInput);
    });

    const displayParamsKeys = Object.keys(indicator.displayParams);
    displayParamsKeys.forEach(displayParamKey => {
        const separatorTr = document.createElement('tr');
        separatorTr.className = 'separator'
        separatorTr.appendChild(document.createElement('td'));
        separatorTr.appendChild(document.createElement('td'));
        table.appendChild(separatorTr);

        const trCategory = document.createElement('tr');
        table.appendChild(trCategory);

        const tdCategoryLabel = document.createElement('td');
        tdCategoryLabel.innerHTML = displayParamKey;
        trCategory.appendChild(tdCategoryLabel);
        trCategory.appendChild(document.createElement('td'));

        const subDisplayParamsKeys = Object.keys(indicator.displayParams[displayParamKey]);
        subDisplayParamsKeys.forEach(subDisplayParamsKey => {
            const tr = document.createElement('tr');
            table.appendChild(tr);
    
            const tdLabel = document.createElement('td');
            tdLabel.innerHTML = subDisplayParamsKey;
            tr.appendChild(tdLabel);
    
            const tdInput = document.createElement('td');
            const input = document.createElement('input');
            input.setAttribute('value', indicator.displayParams[displayParamKey][subDisplayParamsKey]);
            input.onchange = function (ev) {
                indicator.displayParams[displayParamKey][subDisplayParamsKey] = input.value;
                updateIndicatorData(indicator);
            }
            tdInput.appendChild(input);
            tr.appendChild(tdInput);
        });
    });
    document.getElementById('dialogSettings').classList.remove('hide')
    document.getElementById('dialogSettings').setAttribute('style', 'left: ' + (MAX_CANVAS_WIDTH / 2 - 200) + 'px; top: 100px;');
    document.getElementById('dialogSettings').onmouseleave = function(ev) {
        document.getElementById('dialogSettings').classList.add('hide');
    }

}

function initDialog(dropdownData, mx, my) {
    const options = document.getElementById('dialogOptions');
    options.innerHTML = '';

    for (let i = 0; i < dropdownData.values.length; ++i) {
        const tr = document.createElement('tr');
        const tdLeft = document.createElement('td');
        tdLeft.onclick = function(ev) {
            dropdownData.onSelect(dropdownData, dropdownData.values[i]);
        }
        tr.appendChild(tdLeft);
        const td = document.createElement('td');
        const label = dropdownData.labels[i] === undefined ? dropdownData.values[i] : dropdownData.labels[i];
        td.innerHTML = dropdownData.getLabelFromId(dropdownData, i);
        td.onclick = function(ev) {
            dropdownData.onSelect(dropdownData, dropdownData.values[i]);
        }
        tr.appendChild(td);
        const tdRight = document.createElement('td');
        tdRight.onclick = function(ev) {
            dropdownData.onSelect(dropdownData, dropdownData.values[i]);
        }
        tr.appendChild(tdRight);
        options.appendChild(tr);
    }
    document.getElementById('dialog').classList.remove('hide')
    document.getElementById('dialog').setAttribute('style', 'left: ' + mx + 'px; top: ' + my + 'px; lol');
    document.getElementById('dialog').onmouseleave = function(ev) {
        document.getElementById('dialog').classList.add('hide');
    }
}

const currencyPairsDropdown = {
    values: [],
    labels: [],
    selected: DEFAULT_CURRENCY_PAIR,
    getLabelFromId: (self, idx) => {
        if (self.labels[idx] !== undefined) {
            return self.labels[idx];
        }
        return self.getLabel(self, self.values[idx]);
    },
    getLabel: (self, v) => {
        return v.base + "_" + v.target;
    },
    onSelect: (self, v) => {
        document.getElementById('dialog').classList.add('hide');
        self.selected = v;
        uiUpdateCurrencyPairs(v);
        prepend = false;
        document.getElementById('pageId').value = 'last';
        initData();
    },
}

const periodDropdown = {
    values: ['DAILY', 'FOUR_HOURS', 'TWO_HOURS', 'THIRTY_MINUTES', 'FIFTEEN_MINUTES', 'FIVE_MINUTES'],
    labels: ['1D', '4H', '2H', '30', '15', '5'],
    selected: DEFAULT_PERIOD,
    getLabelFromId: (self, idx) => {
        return self.labels[idx];
    },
    getLabel: (self, v) => {
        return self.labels[self.values.indexOf(v)];
    },
    onSelect: (self, v) => {
        document.getElementById('dialog').classList.add('hide');
        self.selected = v;
        uiUpdatePeriod(self.getLabel(self, v));
        prepend = false;
        document.getElementById('pageId').value = 'last';
        initData();
    },
    
}

const addIndicatorDropdown = {
    values: [],
    labels: [],
    selected: undefined,
    getLabelFromId: (self, idx) => {
        return self.labels[idx];
    },
    getLabel: (self, v) => {
        return self.labels[self.values.indexOf(v)];
    },
    onSelect: (self, v) => {
        document.getElementById('dialog').classList.add('hide');
        addIndicator(v);
    },
    
}

function uiUpdateHoveredData(idx) {
    const candle = candles[idx];
    if (candle !== undefined) {
        ohlc = ['open', 'high', 'low', 'close'];
        ohlc.forEach(key => {
            const label = document.getElementById(key + 'CandleLabel');
            label.innerHTML = Number.parseFloat(candle[key]).toFixed(8);
        });
        indicators.forEach(indicator => {
            if (indicator.data[idx] !== undefined) {
                Object.keys(indicatorTemplates[indicator.type].display.charts).forEach(dataKey => {
                    const id = 'indicator' + indicator.uniq + 'data' + dataKey;
                    const label = document.getElementById(id);
                    if (label !== undefined) {
                        detector = indicatorTemplates[indicator.type].display.charts[dataKey].type === 'detector' && indicator.data[idx][dataKey] !== undefined;
                        label.innerHTML = detector ? indicator.data[idx][dataKey].name : Number.parseFloat(indicator.data[idx][dataKey]).toFixed(8);
                    }
                });
            }
        })
    }
}

function uiUpdateCurrencyPairs(pair) {
    document.getElementById('currencyPairSelected').innerHTML = currencyPairsDropdown.getLabel(currencyPairsDropdown, pair); 
}

function uiUpdatePeriod(period) {
    document.getElementById('periodSelected').innerHTML = period;
}