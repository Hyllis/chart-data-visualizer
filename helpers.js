function valueToY(chart, price) {
  return 10 + (chart.height - ((price - chart.scaleMin) / (chart.scaleMax - chart.scaleMin) * chart.height));
}

function yToValue(chart, y) {
  return Number.parseFloat(chart.scaleMax - ((chart.scaleMax - chart.scaleMin) / chart.height * y)).toFixed(8);
}

function priceToPos(price) {
  return valueToY(charts.candles, price);
}

function candleTopPrice(candle) {
  return candle.open > candle.close ? candle.open : candle.close;
}

function candleXPos(idx) {
  return (idx - conf.showCandleFrom) * conf.candleWidth;
}

function candleHeight(candle) {
  return Math.abs(priceToPos(candle.open) - priceToPos(candle.close));
}

function candleMinMaxHeight(candle) {
  return priceToPos(candle.high) - priceToPos(candle.low);
}

function inRangeRound(value, offset, min, max) {
  min= -20;
  if (value + offset < min) r = min;
  else if (value + offset > max) r = max;
  else r = value + offset;
  return Math.round(r);
}
