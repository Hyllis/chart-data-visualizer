let currencyPairs = [];

function initCurrencyPairs() {
    base = currencyPairsDropdown.selected !== undefined ? currencyPairsDropdown.selected.base : 'BTC';
    target = currencyPairsDropdown.selected !== undefined ? currencyPairsDropdown.selected.target : 'ETH';
    period = periodDropdown.selected !== undefined ? periodDropdown.selected : 'DAILY';
    pageId = document.getElementById('pageId').value;
    return new Promise((resolve, reject) => {
        fetch(API_URL + '/pairs')
        .then(response => response.json())
        .then(data => {
            currencyPairs = data;
            resolve();
        })
        .catch(error => console.error(error))
    });
    return promise;
}
