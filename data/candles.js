let candles = [];

function initData() {
  base = currencyPairsDropdown.selected.base;
  target = currencyPairsDropdown.selected.target;
  period = periodDropdown.selected;
  pageId = document.getElementById('pageId').value;
  return new Promise((resolve, reject) => {
    const uri = API_URL + base + '/' + target + '/' + period + '/candles/page/' + pageId;
    console.log("fetching " + uri);
    fetch(uri)
      .then(response => response.json())
      .then(data => {
        if (prepend) {
          candles = data.values.concat(candles);
          conf.showCandleFrom += data.values.length;
        } else {
          candles = data.values;
          conf.showCandleFrom = Math.trunc(candles.length - (conf.showCandleCount * 0.9));
        }
        updatePageInput(data.pagination.max_page, data.pagination.current_page, data.pagination.total);
        refreshDisplay();

        if (indicators.length === 0) {
            addIndicator('Volume');
            addIndicator('BB');
            // addIndicator('StdDev');
            addIndicator('MACD');
            // addIndicator('Volatility');
            // addIndicator('ATR');
            addIndicator('RSI');
            // addIndicator('MinMax');
            addIndicator('PatternDetector');
            addIndicator('EMA', {length: 200, mode: 'close'}, {value: {color: 'BLUE', lineWidth: 3, offset: 0}});
            // addIndicator('EMA', {length: 50, mode: 'close'}, {value: {color: 'PURPLE', lineWidth: 3, offset: 0}});

        } else {
            indicators.forEach(updateIndicatorData);
        }
        resolve();
      })
      .catch(error => console.error(error))
  });
}

setTimeout(function (ev){
  addIndicator('StrategyExecutor');

}, 1000);
