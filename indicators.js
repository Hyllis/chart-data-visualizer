function getIndicatorShortName(indicator) {
    params = Object.values(indicator.params);
    if (indicatorTemplates[indicator.type].meta) {
        params_str = params[0] + ' (' + params[1] + ')'; // TODO replace '|' par ', ' sur [1]
    } else {
        params_str = params.join(', ');
    }
    params_str = params.length > 0 ? ' (' + params_str + ')' : params_str;
    return indicator.type + params_str;
}
function getIndicatorId(indicator) {
    return indicator.type + '_' + indicator.uniq; //Object.values(indicator.params).join('_');
}
function getIndicatorStr(indicator) {
    return indicator.type + '_' + Object.values(indicator.params).join('_');
}


const indicatorTemplates = {
    'Volume': {
        name: 'Volume',
        params: [
            {
                name: 'length',
                default: 9,
            },
            {
                name: 'min_max_length',
                default: 22,
            },
        ],
        display: {
            external: true,
            charts: {
                value: {
                    type: 'histogram',
                    defaultDisplay: {
                        color: 'SECONDARY',
                    },
                },
                agitation: {
                    type: 'histogram',
                    defaultDisplay: {
                        color: 'TURQUOISE',
                    },
                },
                slow: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'RED',
                        lineWidth: 3,
                    },
                },
                fast: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'BLUE',
                        lineWidth: 3,
                    },
                },
            },
        },
    },
    'PatternDetector': {
        name: 'Pattern Detector',
        params: [
        ],
        display: {
            external: false,
            charts: {
                detected: {
                    type: 'detector',
                    defaultDisplay: {
                        color: 'SECONDARY',
                        colorBullish: 'GREEN',
                        colorBearish: 'RED',
                        colorContinuation: 'SECONDARY',
                        alpha: 0.2,
                        lineWidth: 3,
                        offset: 0,
                    },
                },
            },
        },
    },
    'MA': {
        name: 'Moving Average',
        params: [
            {
                name: 'length',
                default: 9,
            },
            {
                name: 'mode',
                default: 'close',
            },
        ],
        display: {
            external: false,
            charts: {
                value: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'SECONDARY',
                        lineWidth: 3,
                        offset: 0,
                    },
                },
            },
        },
    },
    'MinMax': {
        name: 'Min Max',
        params: [
            {
                name: 'length',
                default: 100,
            },
        ],
        display: {
            external: false,
            charts: {
                min: {
                    type: 'dash',
                    defaultDisplay: {
                        color: 'RED',
                        lineWidth: 2,
                    },
                },
                max: {
                    type: 'dash',
                    defaultDisplay: {
                        color: 'GREEN',
                        lineWidth: 2,
                    },
                },
            },
        },
    },
    'EMA': {
        name: 'Exponential Moving Average',
        params: [
            {
                name: 'length',
                default: 9,
            },
            {
                name: 'mode',
                default: 'close',
            },
        ],
        display: {
            external: false,
            charts: {
                value: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'PURPLE',
                        lineWidth: 3,
                        offset: 0,
                    },
                },
            },
        },
    },
    'Alligator': {
        name: 'Alligator',
        params: [
            {
                name: 'jaw_length',
                default: 21,
            },
            {
                name: 'teeth_length',
                default: 13,
            },
            {
                name: 'lips_length',
                default: 8,
            },
        ],
        display: {
            external: false,
            charts: {
                jaw: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'BLUE',
                        lineWidth: 3,
                        offset: 0,
                    },
                },
                teeth: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'RED',
                        lineWidth: 3,
                        offset: 0,
                    },
                },
                lips: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'GREEN',
                        lineWidth: 3,
                        offset: 0,
                    },
                },
            },
        },
    },
    'StdDev': {
        name: 'Standard Deviation',
        params: [
            {
                name: 'length',
                default: 20,
            },
            {
                name: 'mode',
                default: 'close',
            },
        ],
        display: {
            external: true,
            charts: {
                value: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'SECONDARY',
                        lineWidth: 3,
                    },
                },
            },
        },
    },
    'RSI': {
        name: 'Relative Strength Index',
        params: [
            {
                name: 'length',
                default: 14,
            },
            {
                name: 'top',
                default: 70,
            },
            {
                name: 'bottom',
                default: 30,
            },
        ],
        display: {
            external: true,
            scaleMin: 0,
            scaleMax: 100,
            charts: {
                rsi: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'SECONDARY',
                        lineWidth: 2,
                    },
                },
                top: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'PINK',
                        lineWidth: 1,
                    },
                },
                bottom: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'PINK',
                        lineWidth: 1,
                    },
                },
                polygon: {
                    type: 'polygon',
                    data: ['top', 'bottom'],
                    defaultDisplay: {
                        color: 'PINK',
                        alpha: 0.05,
                    },
                },
            },
        },
    },
    'BB': {
        name: 'Bollinger Bands',
        params: [
            {
                name: 'length',
                default: 20,
            },
            {
                name: 'mult',
                default: 2,
            },
        ],
        display: {
            external: false,
            charts: {
                middle: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'PURPLE_BLUE',
                        lineWidth: 2,
                    },
                },
                upper: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'BLUE_SKY',
                        lineWidth: 1,
                    },
                },
                lower: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'BLUE_SKY',
                        lineWidth: 1,
                    },
                },
                polygon: {
                    type: 'polygon',
                    data: ['upper', 'lower'],
                    defaultDisplay: {
                        color: 'PURPLE_BLUE',
                        alpha: 0.1,
                    },
                },
            },
        },
    },
    'MACD': {
        name: 'MACD',
        params: [
            {
                name: 'fast',
                default: 12,
            },
            {
                name: 'slow',
                default: 26,
            },
            {
                name: 'mode',
                default: 'close',
            },
            {
                name: 'signal',
                default: 9,
            },
        ],
        display: {
            external: true,
            charts: {
                macd: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'BLUE',
                        lineWidth: 3,
                    },
                },
                signal: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'RED',
                        lineWidth: 3,
                    },
                },
                histogram: {
                    type: 'histogram',
                    defaultDisplay: {
                        color: 'PINK',
                    },
                },
            },
        },
    },
    'TR': {
        name: 'True Range',
        params: [
        ],
        display: {
            external: true,
            charts: {
                value: {
                    type: 'histogram',
                    defaultDisplay: {
                        color: 'BLUE_SKY',
                    },
                },
            },
        },
    },
    'ATR': {
        name: 'Average True Range',
        params: [
            {
                name: 'length',
                default: 14,
            },
        ],
        display: {
            external: true,
            charts: {
                tr: {
                    type: 'histogram',
                    defaultDisplay: {
                        color: 'SECONDARY',
                        alpha: 0.4,
                    },
                },
                value: {
                    type: 'simple',
                    defaultDisplay: {
                        color: 'SECONDARY',
                        lineWidth: 3,
                    },
                },
            },
        },
    },
    'StrategyExecutor': {
        name: 'Strategy Executor',
        meta: true,
        params: [
            {
                name: 'strategy_name',
                default: 'Bollinger',
            },
            {
                name: 'strategy_params',
                default: '1.4',
            },
        ],
        display: {
            external: false,
            charts: {
                all: {
                    type: 'marker',
                    defaultDisplay: {
                        color: 'white',
                        size: 4,
                    },
                },
            },
        },
    },
    'Volatility': {
        name: 'Volatility Detector',
        meta: true,
        params: [
            {
                name: 'indicator_name',
                default: 'ATR',
            },
            {
                name: 'indicator_params',
                default: '14',
            },
        ],
        display: {
            external: true,
            charts: {
                value: {
                    type: 'histogram',
                    defaultDisplay: {
                        color: 'TURQUOISE',
                    },
                },
            },
        },
    },
};

const indicators = [];

function buildIndicator(type, params, displayParams) {
    const template = indicatorTemplates[type];
    if (template === undefined) {
        alert("No indicator temeplate named: " + type);
        return null;
    } else {
        if (params === undefined) {
            params = {};
            for (let i = 0; i < template.params.length; ++i) {
                params[template.params[i].name] = template.params[i].default;
            }
        }
        if (displayParams === undefined) {
            displayParams = {};
            Object.keys(template.display.charts).forEach(chartKey => {
                displayParams[chartKey] = JSON.parse(JSON.stringify(template.display.charts[chartKey].defaultDisplay));
            })
            // displayParams = JSON.parse(JSON.stringify(template.defaultDisplayParams));
        }
        const indicator = {
            type: type,
            params: params,
            displayParams: displayParams,
            data: [],
            uniq: Math.trunc(Math.random() * 10000000),
            show: true,
        };
        indicator.getId = () => getIndicatorId(indicator);
        indicator.getStr = () => getIndicatorStr(indicator);
        indicator.getShortName = () => getIndicatorShortName(indicator);
        updateIndicatorData(indicator);
        return indicator;
    }
}

function updateIndicatorData(indicator) {
    base = currencyPairsDropdown.selected.base;
    target = currencyPairsDropdown.selected.target;
    period = periodDropdown.selected;
    pageId = document.getElementById('pageId').value;
    initIndicatorData(base, target, period, pageId, indicator);
}

function addIndicator(type, params, displayParams) {
    indicator = buildIndicator(type, params, displayParams)
    if (indicator !== null) {
        indicators.push(indicator);
    }
}

function removeIndicator(indicator) {
    indicators.splice(indicators.indexOf(indicator), 1);
    refreshDisplay();
}

function initIndicatorData(base, target, period, pageId, indicator) {
    fetch(API_URL+base+'/'+target+'/'+period+'/indicator/'+indicator.getStr()+'/page/'+pageId)
        .then(response => response.json())
        .then(data => {
            if (prepend) {
                indicator.data = data.data.concat(indicator.data);
            } else {
                indicator.data = data.data;                
            }
            if (indicatorTemplates[indicator.type].display.external) {
                addIndicatorCanvas(indicator.getId());
                updateCanvasScale(charts.indicators[indicator.getId()], indicator.type, data.data);
            }
            uiInitIndicators();
            refreshDisplay();
        })
        .catch(error => console.error(error))
}

// function initIndicators(base, target, period, pageId) {
//     indicators.forEach(indicator => {
//         initIndicatorData(base, target, period, pageId, indicator)
//     });
// }

// updateIndicator(0, 'MA', {length: 5, mode: 'close'}, {values: {color: 'pink', lineWidth: 3}});
