const charts = {
    candles: {
      c: undefined,
      ctx: undefined,
      cScale: undefined,
      ctxScale: undefined,
      svg: undefined,
      scaleMin: 0,
      scaleMax: 10,
      scale: [],
      height: 400,
    },
    indicators: {
        
    },
    timeScale: {
        c: undefined,
        ctx: undefined,
        height: 44,
    },
};

function updateCandlesCanvasScale() {
    conf.candleMin = 9999999999;
    conf.candleMax = 0;
    // TODO call twice par déplacement
    for (let i = conf.showCandleFrom; i < conf.showCandleFrom + conf.showCandleCount && i < candles.length; ++i) {
        if (candles[i] !== undefined) {
            conf.candleMin = Math.min(conf.candleMin, candles[i].low);
            conf.candleMax = Math.max(conf.candleMax, candles[i].high);
        }
    }
    charts.candles.scaleMin = conf.candleMin * (1 - conf.yScaleModifier / 100);
    charts.candles.scaleMax = conf.candleMax * (1 + conf.yScaleModifier / 100);
}

function updateAllCanvasScale() {
    for (let i = 0; i < Object.keys(charts.indicators).length; ++i) {
        const chartIndicatorKey = Object.keys(charts.indicators)[i];
        const dataIndicator = indicators.filter(indicator => indicator.getId() === chartIndicatorKey)[0];
        updateCanvasScale(charts.indicators[chartIndicatorKey], dataIndicator.type, dataIndicator.data);

    }
}

function updateCanvasScale(chart, indicatorType, data) {
    const displayTemplate = indicatorTemplates[indicatorType].display;
    chart.scaleMin = displayTemplate.scaleMin === undefined ? 9999999999 : displayTemplate.scaleMin;
    chart.scaleMax = displayTemplate.scaleMax === undefined ? -9999999999 : displayTemplate.scaleMax;
    if (displayTemplate.scaleMin === undefined || displayTemplate.scaleMax === undefined) {
        Object.keys(indicatorTemplates[indicatorType].display.charts).forEach(dataKey => {
            // for indicators having values displayed both on an external canvas, and on the main one
            if (displayTemplate.external) {
                for (let i = conf.showCandleFrom; i < conf.showCandleFrom + conf.showCandleCount && i < data.length; ++i) {
                    if (data[i] !== undefined && data[i][dataKey] !== null) {
                        chart.scaleMin = Math.min(chart.scaleMin, data[i][dataKey]);
                        chart.scaleMax = Math.max(chart.scaleMax, data[i][dataKey]);
                    }
                }
            } else {
                throw new Error("???");
                // for (let i = conf.showCandleFrom; i < conf.showCandleFrom + conf.showCandleCount && i < data[dataKey].length; ++i) {
                    // charts.candles.scaleMin = Math.min(charts.candles.scaleMin, 0); //data[dataKey][i]);
                    // charts.candles.scaleMax = Math.max(charts.candles.scaleMax, data[dataKey][i]);
                // }
            }
        });
    }
}

function addIndicatorCanvas(indicatorId) {
    const canvasIndicator = {
        c: undefined,
        ctx: undefined,
        cScale: undefined,
        ctxScale: undefined,
        scaleMin: -20,
        scaleMax: 20,
        scale: [],
        height: 70,
    };
    charts.indicators[indicatorId] = canvasIndicator;
    initCanvas();
    initUi();
}

function buildChartBox(key, chart, className) {
    const chartBox = document.createElement('div');
    chartBox.className = className;

    chart.c = document.createElement('canvas');
    chart.c.id = 'canvas' + key;
    chart.c.className = 'left';
    chart.c.setAttribute('width', MAX_CANVAS_WIDTH - conf.scaleCanvasWidth - 2);
    chart.c.setAttribute('height', chart.height + TOP_OFFSET + BOTTOM_OFFSET);
    chart.ctx = chart.c.getContext('2d');
    chart.cScale = document.createElement('canvas');
    chart.cScale.id = 'canvas' + key + 'scale';
    chart.cScale.className = 'right';
    chart.cScale.setAttribute('width', conf.scaleCanvasWidth);
    chart.cScale.setAttribute('height', chart.height + TOP_OFFSET + BOTTOM_OFFSET);
    chart.ctxScale = chart.cScale.getContext('2d');
    // chart.svg =document.createElementNS("http://www.w3.org/2000/svg", 'svg');
    // chart.svg.id = 'svg' + key;
    // chart.svg.setAttribute('width', MAX_CANVAS_WIDTH + 'px');
    // chart.svg.setAttribute('height', (chart.height + TOP_OFFSET + BOTTOM_OFFSET) + 'px');
    // chart.svg.setAttribute('style', 'width: ' + MAX_CANVAS_WIDTH + 'px; height: ' + (chart.height + TOP_OFFSET + BOTTOM_OFFSET) + 'px; left: 9px');

    chartBox.appendChild(chart.c);
    chartBox.appendChild(chart.cScale);
    // chartBox.appendChild(chart.svg);

    return chartBox;
}

function initCanvas() {
    const tradingViewBox = document.getElementById('tradingViewBox');
    tradingViewBox.innerHTML = '';
    tradingViewBox.appendChild(buildChartBox('candles', charts.candles, 'candlesBox'));
    Object.keys(charts.indicators).forEach(indicatorKey => {
        tradingViewBox.appendChild(buildChartBox(indicatorKey, charts.indicators[indicatorKey], 'indicatorBox'));
    });
    tradingViewBox.appendChild(buildChartBox('timeScale', charts.timeScale, 'indicatorBox'));
}
