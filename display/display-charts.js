
function drawCandle(ctx, candle, idx) {
    ctx.beginPath();
    ctx.fillStyle = candle.open > candle.close ? COLOR('RED') : COLOR('GREEN');
    ctx.rect(candleXPos(idx) + conf.candleWidth / 2 - 0.5, priceToPos(candle.low), 1, candleMinMaxHeight(candle));
    ctx.fill();
    ctx.beginPath();
    ctx.rect(candleXPos(idx) + 1, priceToPos(candleTopPrice(candle)), conf.candleWidth - 2, candleHeight(candle));
    ctx.fill();
}

function refreshScaleDisplay(chart) {
    updateScaleData(chart)
    chart.scale.forEach((value, idx) => {
        chart.ctxScale.beginPath();
        y = valueToY(chart, value);
        chart.ctxScale.fillStyle = COLOR('SECONDARY');
        chart.ctxScale.rect(0, y - 1, 6, 2);
        chart.ctxScale.fill();
        chart.ctxScale.font = '10px Roboto'
        chart.ctxScale.fillText(Number.parseFloat(value).toFixed(8), 10, y + 3);
    });
}

function displayTimeScale() {
    charts.timeScale.ctx.beginPath();
    charts.timeScale.ctx.fillStyle = COLOR('PRIMARY');
    charts.timeScale.ctx.font = '10px Roboto'
    charts.timeScale.ctx.rect(0, 0, MAX_CANVAS_WIDTH + 2, charts.timeScale.height);
    charts.timeScale.ctx.fill();
    charts.timeScale.scale.forEach((value, idx) => {
      charts.timeScale.ctx.beginPath();
      x = candleXPos(value + conf.showCandleFrom) + conf.candleWidth / 2;
      charts.timeScale.ctx.fillStyle = COLOR('SECONDARY');
      charts.timeScale.ctx.rect(x - 1, 0, 2, 6);
      charts.timeScale.ctx.fill();
      const candle = candles[conf.showCandleFrom + value];
      const candleDate = candle !== undefined ? candle.date : 0;
      if (candleDate > 0) {
        const isoDate = new Date(candleDate * 1000).toISOString();
        const date = isoDate.substring(0, 10);
        const hour = isoDate.substring(11, 19);
        charts.timeScale.ctx.fillText(date, x - 30, 20);
        charts.timeScale.ctx.fillText(hour, x - 19, 32);
      }
    });
}

function clearChart(chart) {
    chart.ctx.beginPath();
    chart.ctx.fillStyle = COLOR('PRIMARY');
    chart.ctx.rect(0, 0, MAX_CANVAS_WIDTH, chart.height + TOP_OFFSET + BOTTOM_OFFSET);
    chart.ctx.fill();
    chart.scale.forEach((value, idx) => {
      chart.ctx.beginPath();
      y = valueToY(chart, value);
      chart.ctx.fillStyle = COLOR('SECONDARY', 0.2);
      chart.ctx.rect(0, y - 1, MAX_CANVAS_WIDTH, 1);
      chart.ctx.fill();
    });
    charts.timeScale.scale.forEach((value, idx) => {
      chart.ctx.beginPath();
      x = candleXPos(value + conf.showCandleFrom) + conf.candleWidth / 2;
      chart.ctx.fillStyle = COLOR('SECONDARY', 0.2);
      chart.ctx.rect(x - 0.5, 0, 1, chart.height + TOP_OFFSET + BOTTOM_OFFSET);
      chart.ctx.fill();
    });
    chart.ctxScale.beginPath();
    chart.ctxScale.fillStyle = COLOR('PRIMARY');
    chart.ctxScale.rect(0, 0, conf.scaleCanvasWidth, chart.height + TOP_OFFSET + BOTTOM_OFFSET);
    chart.ctxScale.fill();
}

function clearAllCharts(chart) {
    clearChart(charts.candles);
    Object.values(charts.indicators).forEach(indicator => clearChart(indicator));
}

function refreshMainDisplay(ctx) {
    for (let i = conf.showCandleFrom; i < conf.showCandleFrom + conf.showCandleCount; ++i) {
      if (candles[i] !== undefined) {
        drawCandle(charts.candles.ctx, candles[i], i);
      }
    }  
}

function refreshSimpleIndicator(chart, display, data) {
    if (data.length >  0) {
      chart.ctx.beginPath();
      chart.ctx.strokeStyle = COLOR(display.color, display.alpha);
      chart.ctx.lineWidth = display.lineWidth;
      offset = display.offset === undefined ? 0 : display.offset;
      let first = true;
      for (let i = conf.showCandleFrom; i <= conf.showCandleFrom + conf.showCandleCount; ++i) {
        if (data[i] !== null) {
          if (first) {
            chart.ctx.moveTo( candleXPos(i + parseInt(offset)) + conf.candleWidth / 2 - 0.5, valueToY(chart, data[i]) );
            first = false;
          } else {
            chart.ctx.lineTo( candleXPos(i + parseInt(offset)) + conf.candleWidth / 2 - 0.5, valueToY(chart, data[i]) );
          }
        }
      }
      chart.ctx.stroke();
    }
}

function refreshDashIndicator(chart, display, data) {
    if (data.length >  0) {
      chart.ctx.beginPath();
      chart.ctx.strokeStyle = COLOR(display.color, display.alpha);
      chart.ctx.lineWidth = display.lineWidth;
      for (let i = conf.showCandleFrom; i <= conf.showCandleFrom + conf.showCandleCount; ++i) {
        if (data[i] !== null) {
          x = candleXPos(i);
          y = valueToY(chart, data[i]);
          chart.ctx.moveTo(x, y);
          chart.ctx.lineTo(x + conf.candleWidth, y);
        }
      }
      chart.ctx.stroke();
    }
}

function refreshHistogramIndicator(chart, display, data) {
    if (data.length >  0) {
      chart.ctx.beginPath();
      for (let i = conf.showCandleFrom; i <= conf.showCandleFrom + conf.showCandleCount; ++i) {
        yData = valueToY(chart, data[i]);
        yZero = valueToY(chart, 0);
        y = yData > yZero ? yZero : yData;
        height = Math.abs(yData - yZero);
        chart.ctx.fillStyle = COLOR(display.color, display.alpha);
        chart.ctx.rect(candleXPos(i) + 2, y, conf.candleWidth - 4, height);
      }
      chart.ctx.fill();
    }
}

function refreshDetectorIndicator(chart, display, data) {
    if (data.length >  0) {
      for (let i = conf.showCandleFrom; i <= conf.showCandleFrom + conf.showCandleCount; ++i) {
        if (data[i] !== undefined) {
          chart.ctx.beginPath();
          w = conf.candleWidth / 2 * data[i].strength;
          x = candleXPos(i) + conf.candleWidth / 2 - w / 2;
          candle = candles[i];
          if (data[i].direction === 'BULLISH') {
            y = 0;
            chart.ctx.fillStyle = COLOR(display.colorBullish, 0.2);
            h = valueToY(chart, candle['open'] > candle['close'] ? candle['open'] : candle['close']);
          } else if (data[i].direction === 'BEARISH') {
            y = valueToY(chart, candle['open'] > candle['close'] ? candle['close'] : candle['open']);
            chart.ctx.fillStyle = COLOR(display.colorBearish, 0.2);
            h = 9999;
          } else {
            y = 0;
            chart.ctx.fillStyle = COLOR(display.colorContinuation, 0.2);
            h = 9999;
          }
          chart.ctx.rect(x, y, w, h);
          chart.ctx.fill();
        }
      }
    }
}

function refreshMarkerIndicator(chart, display, data, allData) {
  for (let i = conf.showCandleFrom; i <= conf.showCandleFrom + conf.showCandleCount && i < data.length; ++i) {
    x = candleXPos(i) + conf.candleWidth / 2;
    if (allData !== undefined && allData[i] !== undefined) {
      // yEntry
      if (allData[i]['entry'] !== undefined) {
        yEntry = valueToY(chart, allData[i]['entry']);
        chart.ctx.beginPath();
        chart.ctx.strokeStyle = COLOR(display.color, display.alpha);
        chart.ctx.lineWidth = display.size / 2;
        chart.ctx.moveTo(x - display.size, yEntry - display.size);
        chart.ctx.lineTo(x + display.size, yEntry + display.size);
        chart.ctx.moveTo(x - display.size, yEntry + display.size);
        chart.ctx.lineTo(x + display.size, yEntry - display.size);
        chart.ctx.stroke();
        chart.ctx.strokeStyle = COLOR('BLUE_SKY');
        chart.ctx.lineWidth = 1.5;
        chart.ctx.beginPath();
        chart.ctx.moveTo(x, 0);
        chart.ctx.lineTo(x, chart.height + TOP_OFFSET + BOTTOM_OFFSET);
        chart.ctx.stroke();

      }

      // yTP
      if (allData[i]['take_profit'] !== undefined) {
        yTP = valueToY(chart, allData[i]['take_profit']);
        chart.ctx.beginPath();
        chart.ctx.strokeStyle = 'green';
        chart.ctx.lineWidth = display.size / 2;
        chart.ctx.moveTo(x - display.size, yTP - display.size);
        chart.ctx.lineTo(x + display.size, yTP + display.size);
        chart.ctx.moveTo(x - display.size, yTP + display.size);
        chart.ctx.lineTo(x + display.size, yTP - display.size);
        chart.ctx.stroke();
      }

      // ySL
      if (allData[i]['stop_loss'] !== undefined) {
        ySL = valueToY(chart, allData[i]['stop_loss']);
        chart.ctx.beginPath();
        chart.ctx.strokeStyle = 'red';
        chart.ctx.lineWidth = display.size / 2;
        chart.ctx.moveTo(x - display.size, ySL - display.size);
        chart.ctx.lineTo(x + display.size, ySL + display.size);
        chart.ctx.moveTo(x - display.size, ySL + display.size);
        chart.ctx.lineTo(x + display.size, ySL - display.size);
        chart.ctx.stroke();
      }

      if (allData[i]['after'] !== undefined) {
        yEntry = valueToY(chart, allData[i]['entry']);
        yTP = valueToY(chart, allData[i]['take_profit']);
        ySL = valueToY(chart, allData[i]['stop_loss']);
        xDest = candleXPos(i + allData[i]['after']) + conf.candleWidth / 2;

        chart.ctx.beginPath();
        chart.ctx.fillStyle = COLOR('DARK_GREEN', 0.2)
        chart.ctx.rect(x, yEntry, xDest - x, yTP - yEntry);
        chart.ctx.fill();

        chart.ctx.beginPath();
        chart.ctx.fillStyle = COLOR('DARK_RED', 0.2)
        chart.ctx.rect(x, ySL, xDest - x, yEntry - ySL);
        chart.ctx.fill();
      }

      if (allData[i]['status'] !== undefined) {
        xDest = candleXPos(i + allData[i]['status']['after']) + conf.candleWidth / 2;

        // SL/TP box
        chart.ctx.fillStyle = COLOR('DARK_GREEN', 0.2);
        chart.ctx.rect(x, y, xDest - x, yTP - y);
        chart.ctx.fill();
        chart.ctx.fillStyle = COLOR('DARK_RED', 0.2);
        chart.ctx.rect(x, ySL, xDest - x, y - ySL);
        chart.ctx.fill();

        // line to SL/TP
        chart.ctx.beginPath();
        chart.ctx.strokeStyle = 'white'; // allData[i]['won'] ? COLOR('GREEN') : COLOR('RED');
        chart.ctx.lineWidth = 2;
        chart.ctx.moveTo(x, y);
        chart.ctx.lineTo(xDest, allData[i]['status']['won'] ? yTP : ySL);
        chart.ctx.stroke();

        // yMax
        xMax = candleXPos(i + allData[i]['status']['max_period_after']) + conf.candleWidth / 2;
        yMax = valueToY(chart, allData[i]['status']['max_period']);
        chart.ctx.beginPath();
        chart.ctx.strokeStyle = 'green'; // allData[i]['won'] ? COLOR('GREEN') : COLOR('RED');
        chart.ctx.lineWidth = 5;
        chart.ctx.moveTo(x, y);
        chart.ctx.lineTo(xMax, yMax);
        chart.ctx.stroke();

        // // yMin
        // xMin = candleXPos(i + allData[i]['status']['min_period_after']) + conf.candleWidth / 2;
        // yMin = valueToY(chart, allData[i]['status']['min_period']);
        // chart.ctx.beginPath();
        // chart.ctx.strokeStyle = 'red'; // allData[i]['won'] ? COLOR('GREEN') : COLOR('RED');
        // chart.ctx.lineWidth = 5;
        // chart.ctx.moveTo(x, y);
        // chart.ctx.lineTo(xMin, yMin);
        // chart.ctx.stroke();
      }
    }
  }
}

function refreshPolygonIndicator(chart, display, dataTop, dataBottom) {
    if (dataTop.length >  0) {
      chart.ctx.beginPath();
      chart.ctx.fillStyle = COLOR(display.color, display.alpha);
      chart.ctx.lineWidth = display.lineWidth;
      let first = true;
      for (let i = conf.showCandleFrom; i <= conf.showCandleFrom + conf.showCandleCount; ++i) {
        if (dataTop[i] !== undefined && dataTop[i] !== null) {
          if (first) {
            chart.ctx.moveTo( candleXPos(i) + conf.candleWidth / 2 - 0.5, valueToY(chart, dataTop[i]) );
            first = false;
          } else {
            chart.ctx.lineTo( candleXPos(i) + conf.candleWidth / 2 - 0.5, valueToY(chart, dataTop[i]) );
          }
        }
      }
      for (let i = conf.showCandleFrom + conf.showCandleCount; i >= conf.showCandleFrom; --i) {
        if (dataBottom[i] !== undefined && dataBottom[i] !== null) {
          chart.ctx.lineTo( candleXPos(i) + conf.candleWidth / 2 - 0.5, valueToY(chart, dataBottom[i]) );
        }
      }
      chart.ctx.fill();
    }
}

function displayIndicators() {
    indicators.filter(indicator => indicator.show).forEach(indicator => {
      Object.keys(indicatorTemplates[indicator.type].display.charts).forEach(dataKey => {
        const templateDisplay = indicatorTemplates[indicator.type].display;
        const chart = templateDisplay.external ? charts.indicators[indicator.getId()] : charts.candles;
        if (templateDisplay.charts[dataKey].type === 'simple') {
          const data =  indicator.data.map(data => data[dataKey]);
          refreshSimpleIndicator(chart, indicator.displayParams[dataKey], data);
        } else if (templateDisplay.charts[dataKey].type === 'histogram') {
          const data = indicator.data.map(data => data[dataKey]);
          refreshHistogramIndicator(chart, indicator.displayParams[dataKey], data);
        } else if (templateDisplay.charts[dataKey].type === 'polygon') {
          const keyUpper = templateDisplay.charts[dataKey].data[0];
          const keyLower = templateDisplay.charts[dataKey].data[1];
          const upperData =  indicator.data.map(data => data[keyUpper]);
          const lowerData =  indicator.data.map(data => data[keyLower]);
          refreshPolygonIndicator(chart, indicator.displayParams['polygon'], upperData, lowerData);
        } else if (templateDisplay.charts[dataKey].type === 'marker') {
          const data = indicator.data.map(data => data[dataKey]);
          refreshMarkerIndicator(chart, indicator.displayParams[dataKey], data, dataKey === 'all' ? indicator.data : undefined);
        } else if (templateDisplay.charts[dataKey].type === 'dash') {
          const data =  indicator.data.map(data => data[dataKey]);
          refreshDashIndicator(chart, indicator.displayParams[dataKey], data);
        } else if (templateDisplay.charts[dataKey].type === 'detector') {
          const data =  indicator.data.map(data => data[dataKey]);
          refreshDetectorIndicator(chart, indicator.displayParams[dataKey], data);
        }
      });
    });
}