const API_URL = 'http://localhost:5365/chartdata/';
let prepend = false;

// const COLORS = { PRIMARY: "#d4f3ef", SECONDARY: '#dee3e2', RED: "#ee8572", GREEN: "#63b7af" };
// const COLORS = { PRIMARY: "#ebe5d4", SECONDARY: '#666', RED: "#bb3b0e", GREEN: "#708160" };
// const COLORS = { PRIMARY: "#264653", SECONDARY: '#dee3e2', RED: "#e76f51", GREEN: "#2a9d8f" };
// const COLORS = { PRIMARY: '#393e46', SECONDARY: '#dee3e2', RED: '#f09595', GREEN: '#9dd8c8', BLUE: '#3797a4', YELLOW: '#fcf876' };
const COLORS_RGB = { PRIMARY: '57,62,70', SECONDARY: '222,227,226', RED: '240,149,149', GREEN: '157,216,200', BLUE: '55,161,164', YELLOW: '252,248,118' };
const SATURATION = 75;
const LUMINOSITY = 75;
const DARK_LUMINOSITY = 25;
const COLORS_HSL = {
    PRIMARY: '217,10%,25%',
    SECONDARY: '168,8%,88%',
    RED: '0,'+SATURATION+'%,'+LUMINOSITY+'%',
    DARK_RED: '0,'+SATURATION+'%,'+DARK_LUMINOSITY+'%',
    ORANGE: '30,'+SATURATION+'%,'+LUMINOSITY+'%',
    YELLOW: '60,'+SATURATION+'%,'+LUMINOSITY+'%',
    GREEN_APPLE: '90,'+SATURATION+'%,'+LUMINOSITY+'%',
    GREEN: '120,'+SATURATION+'%,'+LUMINOSITY+'%',
    DARK_GREEN: '120,'+SATURATION+'%,'+DARK_LUMINOSITY+'%',
    TURQUOISE: '150,'+SATURATION+'%,'+LUMINOSITY+'%',
    BLUE_SKY: '180,'+SATURATION+'%,'+LUMINOSITY+'%',
    BLUE: '220,'+SATURATION+'%,'+LUMINOSITY+'%',
    PURPLE_BLUE: '260,'+SATURATION+'%,'+LUMINOSITY+'%',
    PURPLE: '300,'+SATURATION+'%,'+LUMINOSITY+'%',
    PINK: '320,'+SATURATION+'%,'+LUMINOSITY+'%',
    PINK_SALMON: '345,'+SATURATION+'%,'+LUMINOSITY+'%',
};

const COLOR = (color, alpha) => COLORS_HSL[color] === undefined
  ? color : 'hsla(' + COLORS_HSL[color] + ', ' + (alpha === undefined ? 1 : alpha) + ')';

const MAX_CANVAS_WIDTH = 1270;
const DEFAULT_SCALE_CANVAS_WIDTH = 96;
const CANVAS_HEIGHT = 500;
const TOP_OFFSET = 10;
const BOTTOM_OFFSET = 10;

const DEFAULT_CURRENCY_PAIR = {base: 'USDT', target: 'BTC'};
const DEFAULT_PERIOD = 'DAILY';

const conf = {
  yScaleModifier: 0,
  showCandleFrom: 0,
  showCandleCount: 100,
  canvasWidth: MAX_CANVAS_WIDTH - DEFAULT_SCALE_CANVAS_WIDTH,
  candleWidth: (MAX_CANVAS_WIDTH - DEFAULT_SCALE_CANVAS_WIDTH) / 100,
  candleMin: 0.000001,
  candleMax: 10000000,
  scaleCanvasWidth: DEFAULT_SCALE_CANVAS_WIDTH,
  clickStartId: undefined,
  clickStartShowCandleFrom: undefined,
  previousMovement: 0,
  previousHoverId: 0,
}
