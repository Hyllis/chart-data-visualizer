function initHooks() {
  document.getElementById('ui').onkeydown = function (ev) {
    const moveOffset = conf.showCandleCount / 10;
    if (ev.keyCode === 37) { // left
      conf.showCandleFrom = inRangeRound(conf.showCandleFrom, -moveOffset, 0, candles.length - conf.showCandleCount * 0.2);
    } else if (ev.keyCode === 39) { // right
      conf.showCandleFrom = inRangeRound(conf.showCandleFrom, moveOffset, 0, candles.length - conf.showCandleCount * 0.2);
    } else if (ev.keyCode === 109) { // minus
      conf.showCandleCount += 20;
      conf.candleWidth = conf.canvasWidth / conf.showCandleCount;
    } else if (ev.keyCode === 107) { // plus
      conf.showCandleCount -= 20;
      if (conf.showCandleCount < 20) conf.showCandleCount = 20;
      conf.candleWidth = conf.canvasWidth / conf.showCandleCount;
    } else if (ev.keyCode === 38) { // up
      if (conf.yScaleModifier > 0) --conf.yScaleModifier;
      ev.preventDefault();
    } else if (ev.keyCode === 40) { // down
      ++conf.yScaleModifier;
      ev.preventDefault();
    }
    refreshDisplay();
  }

  document.getElementById('ui').onmousewheel = function (ev) {
    const currentId = mouseXToDataId(ev.clientX);
    const scrollSpeed = 12;
    if (ev.deltaY < 0) { // forwards - zoom
      conf.showCandleFrom = inRangeRound(conf.showCandleFrom, (currentId > conf.showCandleCount / 2 ? scrollSpeed : 0), 0, candles.length - conf.showCandleCount * 0.2);
      conf.showCandleCount -= scrollSpeed;
      if (conf.showCandleCount < 20) conf.showCandleCount = 20;
      conf.candleWidth = conf.canvasWidth / conf.showCandleCount;
    } else { // backwards - unzoom
      conf.showCandleFrom = inRangeRound(conf.showCandleFrom, (currentId < conf.showCandleCount / 2 ? scrollSpeed : 0), 0, candles.length - conf.showCandleCount * 0.2);
      conf.showCandleCount += scrollSpeed;
      conf.candleWidth = conf.canvasWidth / conf.showCandleCount;
    }
    refreshDisplay();
    uiShowZone(mouseXToDataId(ev.clientX));
    ev.preventDefault();
  }

  document.getElementById('ui').onmousedown = function (ev) {
    conf.clickStartId = mouseXToDataId(ev.clientX);
    conf.clickStartTime = new Date().getTime();
  }
  document.getElementById('ui').onmousemove = function (ev) {
    const currentId = mouseXToDataId(ev.clientX);
    if (conf.clickStartId !== undefined) {
      const movement = (currentId - conf.clickStartId);
      if (movement !== conf.previousMovement) {
        conf.showCandleFrom = inRangeRound(conf.showCandleFrom, -(movement * 2), 0, candles.length - conf.showCandleCount * 0.2);
        conf.previousMovement = movement;
        refreshDisplay();
      }
    }
    if (currentId !== conf.previousHoverId) {
      if (currentId + conf.showCandleFrom < candles.length) {
        uiUpdateHoveredData(currentId + conf.showCandleFrom);
      }
    }
    conf.previousHoverId = currentId;
    uiShowZone(currentId);
    uiUpdateCursorPrice(ev.clientY);
  }
  document.getElementById('ui').onmouseup = function (ev) {
    conf.clickStartId = undefined;
    conf.clickStartTime = undefined;
  }
}

function mouseXToDataId(x) {
  x -= charts.candles.c.offsetLeft;
  if (x < 0) return 0;
  return Math.trunc(x / conf.candleWidth);
}

function addOption(selectId, optionValue, optionLabel, isSelected) {
  optionLabel = optionLabel === undefined ? optionValue : optionLabel;
  isSelected = isSelected === undefined ? false : isSelected;
  var sel = document.getElementById(selectId);
  var opt = document.createElement('option');
  if (isSelected) {
    opt.setAttribute('selected', true);
  }
  opt.appendChild(document.createTextNode(optionLabel));
  opt.value = optionValue;
  sel.appendChild(opt);
}

function updatePageInput(maxPage, currentPage, total) {
  total = total === undefined ? '?' : total
  document.getElementById('pageId').innerHTML = '';
  addOption('pageId', 'last', 'last', currentPage === 'last');
  addOption('pageId', 'all', 'all (' + total + ')', currentPage === 'all');
  for (let i = 0; i <= maxPage; ++i) {
    addOption('pageId', i, (i + 1) + ' / ' + (maxPage + 1), parseInt(currentPage) === i);
  }
}

function buildIndicatorSelect(selected) {
  const sel = document.createElement('select');
  Object.keys(indicatorTemplates).forEach(indicatorKey => {
    const opt = document.createElement('option');
    opt.appendChild(document.createTextNode(indicatorTemplates[indicatorKey].name));
    opt.value = indicatorKey;
    if (selected === indicatorKey) {
      opt.setAttribute('selected', true);
    }
    sel.appendChild(opt);
  });
  return sel;
}

function appendLabelInputToTr(tr, idx, paramKey, value, isMin) {
  const key = 'indicator' + idx + '_' + paramKey;

  const tdLabel = document.createElement('td');
  tdLabel.className = 'min';
  const label = document.createElement('label');
  label.setAttribute('for', key);
  label.appendChild(document.createTextNode(paramKey));
  tdLabel.appendChild(label);
  tr.appendChild(tdLabel);

  const tdInput = document.createElement('td');
  tdInput.className = isMin ? 'min' : 'max';
  if (!isMin) tdInput.setAttribute('colspan', 99);
  const inpt = document.createElement('input');
  inpt.className = 'short'
  inpt.id = key;
  inpt.name = key;
  inpt.setAttribute('value', value);
  tdInput.appendChild(inpt);
  tr.appendChild(tdInput);

  return inpt;
}


//   <tr>
//   <td class="min"><select class="indicatorType" id="indicator0_type"><option value="MA">Moving Average</option></select></td>
//   <td class="min"><input class="short" type="text" name="indicator0_length" id="indicator0_length"></td></td>
//   <td class="min"><label for="indicator0_mode">mode</label></td><td class="min"><input class="short" type="text" name="indicator0_mode" id="indicator0_mode"></td></td>
//   <td class="min"><label for="indicator0_display">display</label></td><td><input style="width: 100%" type="text" name="indicator0_display" id="indicator0_display"></td></td>
// </tr>

function initInput() {
  document.getElementById('pageId').value = 'last';
  document.getElementById('pageId').onchange = function (ev) {
    initData();
  }
  document.getElementById('yScaleModifier').onchange = function (ev) {
    conf.yScaleModifier = document.getElementById('yScaleModifier').value * 1;
    refreshDisplay();
  }
  document.getElementById('showCandleFrom').onchange = function (ev) {
    conf.showCandleFrom = document.getElementById('showCandleFrom').value * 1;
    refreshDisplay();
  }
  document.getElementById('showCandleCount').onchange = function (ev) {
    conf.showCandleCount = document.getElementById('showCandleCount').value * 1;
    refreshDisplay();
  }
  updatePageInput(1, 'last');
  initHooks();
  // updateIndicatorInputs();
}